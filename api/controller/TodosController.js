'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('../db')

module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM todos.content'
        db.query(sql, (err, response) => {
            if (err) console.log(err+" ")
            res.json(response)
        })
    },
    detail: (req, res) => {
        let sql = 'SELECT * FROM content WHERE id = ?'
        db.query(sql, [req.params.todoId], (err, response) => {
            if (err) throw err
            res.json(response[0])
        })
    },
    update: (req, res) => {
        let data = req.body;
        let todoId = req.params.todoId;
        let sql = 'UPDATE content SET ? WHERE id = ?'
        db.query(sql, [data, todoId], (err, response) => {
            if (err) throw err
            res.json({message: 'Update success!'})
        })
    },
    store: (req, res) => {
        console.log(req.body);
        let data = req.body;
        let sql = 'INSERT INTO content SET ?'
        db.query(sql, [data], (err, response) => {
            if (err) throw err
            res.json({message: 'Insert success!'})
        })
    },
    delete: (req, res) => { 
        let sql = 'DELETE FROM content WHERE id = ?'
        db.query(sql, [req.params.todoId], (err, response) => {
            if (err) throw err
            res.json({message: 'Delete success!'})
        })
    }
}