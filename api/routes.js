'use strict';
module.exports = function (app) {
  let todosCtrl = require('./controller/TodosController');

  // todoList Routes
  app.route('/todos')
    .get(todosCtrl.get)
    .post(todosCtrl.store);

  app.route('/todos/:todoId')
    .get(todosCtrl.detail)
    .put(todosCtrl.update)
    .delete(todosCtrl.delete);
};